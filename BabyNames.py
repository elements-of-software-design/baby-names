#  File: BabyNames.py

#  Description:  Reads a file with baby names and analyzes the data.

#  Student Name:  Christopher Calizzi

#  Student UT EID:  csc3322

#  Course Name: CS 313E

#  Unique Number: 50295

#  Date Created: 2-9-20

#  Date Last Modified: 2-9-20


class BabyNames(object):
    # Initializes the dictionary that will hold all the baby names
    def __init__(self):
        # key: name
        # value: list of ranks
        self.names = {}

    # Reads in the file and adds to the dictionary
    def fill_data(self, file_name):
        try:
            file = open(file_name)
            line = file.readline()
            while line:
                name = line[0:line.index(" ")]
                line = line[line.index(" "):]
                values = []
                while " " in line:
                    line = line[1:len(line)]
                    if " " in line:
                        values.append(int(line[0:line.index(" ")]))
                        line = line[line.index(" "):]
                    #reads in final number
                    elif line:
                        values.append(int(line))
                for i in range(len(values)):
                    if values[i] ==0:
                        values[i] = 1001
                self.names[name] = values
                line = file.readline()
            file.close()
        except:
            print("Error reading file")
    # True if a name exists in the dictionary and False otherwise.
    def contains_name(self, name):
        return name in self.names

    # Returns all the rankings for a given name. Assume the name exists
    def find_ranking(self, name):
        return self.names[name]

    # Returns a list of names that have a rank in all the decades in sorted order by name.
    def ranks_of_all_decades(self):
        ranked = []
        for name,list in self.names.items():
            if not 1001 in list:
                ranked.append(name)
        return ranked

    #  Returns a list of all the names that have a rank in a given decade in order of rank.
    def ranks_of_a_decade(self, decade):
        decade = (decade-1900)//10
        ranked = []
        for name,values in self.names.items():
            if values[decade] < 1001:
                ranked.append([values[decade],name])
        ranked.sort()
        names = []
        for l in ranked:
            names.append(l[1])
        return names

    # Return all names that are getting more popular in every decade. The list must be sorted by name.
    def getting_popular(self):
        more_popular = []
        for name, values in self.names.items():
            increasing = True
            index = 1
            while increasing and index<len(values):
                increasing  = increasing and values[index]<values[index-1]
                index+=1
            if increasing:
                more_popular.append(name)
        return more_popular

    # Return all names that are getting less popular in every decade. The list must be sorted by name.
    def less_popular(self):
        less_popular = []
        for name, values in self.names.items():
            decreasing = True
            index = 1
            while decreasing and index < len(values):
                decreasing = decreasing and values[index] > values[index - 1]
                index += 1
            if decreasing:
                less_popular.append(name)
        return less_popular

def main():
    names = BabyNames()
    names.fill_data("names.txt")
    choice = 1
    names.ranks_of_a_decade(1980)
    while choice !=7:
        try:
            print("Options:\nEnter 1 to search for names.\nEnter 2 to display data for one name.\nEnter 3 to display all names that appear in only one decade.\nEnter 4 to display all names that appear in all decades.\nEnter 5 to display all names that are more popular in every decade.\nEnter 6 to display all names that are less popular in every decade.\nEnter 7 to quit.")
            choice = int(input("Enter choice: "))
            if choice ==1:
                name = input("Enter a name: ")
                if names.contains_name(name):
                    values = names.find_ranking(name)
                    decade = 0
                    lowest_rank = min(values)
                    while values[decade] != lowest_rank:
                        decade += 1
                    decade = 1900 + decade * 10
                    print("\nThe matches with their highest ranking decade are:")
                    print(name,decade,"\n\n")
                else:
                    print()
                    print(name,"does not appear in any decade\n\n")
            elif choice==2:
                name = input("Enter a name: ")
                if names.contains_name(name):
                    values = names.find_ranking(name)
                    for i in range(len(values)):
                        if values[i] ==1001:
                            values[i] = 0
                    print(name, end = ":")
                    for i in values:
                        print("",i,end = "")
                    print()
                    for i in range(len(values)):
                        print(str(1900+i*10)+":",values[i])
                    print("\n")
                else:
                    print()
                    print(name,"does not appear in any decade\n\n")
            elif choice == 3:
                decade = int(input("Enter decade: "))
                ranked = names.ranks_of_a_decade(decade)
                print("The names are in order of rank:")
                for i in range(len(ranked)):
                    print(str(ranked[i]) + ":",names.names[ranked[i]][(decade-1900)//10])
                print("\n")
            elif choice == 4:
                every = names.ranks_of_all_decades()
                print(str(len(every)), "names appear in every decade. The names are:" )
                for name in every:
                    print(name)
                print()
            elif choice == 5:
                popular = names.getting_popular()
                print(len(popular), "names are more popular every decade.")
                for name in popular:
                    print(name)
                print("\n")
            elif choice == 6:
                popular = names.less_popular()
                print(len(popular), "names are less popular every decade.")
                for name in popular:
                    print(name)
                print("\n")
            elif choice == 7:
                print("Goodbye")
            else:
                print("Please enter a number 1-7")

        except:
            print("Error, please enter a valid input\n")
if __name__ == '__main__':
    main()


